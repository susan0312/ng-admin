

# xxx后台登录账号（备用）
- xxx:
* phone: xxxxx
* password: 123456

# 开发文档

## 启动项目

* $ npm i
* $ npm start （ng serve -o）
* $ npm run build （打包、发布）
* $ npm run analyze （分析构建文件体积）

## 登录流程

* login组件内登录，一般不用做token校验（_allow_anonymous=true）

* 密码经过md5加密，保证安全性

* 关于发送请求，内置default拦截器，对每次http请求注入token

## 关于发送请求、后端交互

* 首次启动 Angular 执行 APP_INITIALIZER

* UI 组件交互操作

* 使用封装的 _HttpClient 发送请求

* 触发用户认证拦截器 @delon/auth，统一加入 token 参数(DelonAuthConfig中)

* 若未存在 token 或已过期中断后续请求，直接跳转至登录页

* 触发默认拦截器，统一处理前缀等信息

* 获取服务端返回

* 触发默认拦截器，统一处理请求异常、业务异常等

* 数据更新，并刷新 UI

* 默认情况下 DelonAuthModule.forRoot() 是不会加载任何HTTP拦截器，这需要你手工在你的相应的模块中加上
```
{ provide: HTTP_INTERCEPTORS, useClass: SimpleInterceptor, multi: true }
```

## 新增页面/组件/模块

* 通过命令 `generate` （原生ng6语法）
* ng g ng-alain:module xxx （ng-alain语法）
